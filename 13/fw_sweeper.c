/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

  //            //
 /* Prototypes */
//            //
typedef struct fw firewall;

void parse_args(int, char *[], FILE**);
void usage(void);
int travel_firewalls(firewall*, int[], int, firewall*);
int read_input(FILE*, firewall*, int[]);
void move_scanner(firewall*);
int delay_to_dodge_firewalls(firewall*, int[], int);
void reset_firewall_states(firewall*, int[], int, firewall*);

  //         //
 /* Structs */
//         //
struct fw {
    int direction;
    int xpos;
    int ypos;
    int range;
};

  //           //
 /* Constants */
//           //
const char * const PROGRAM_NAME = "fw_sweeper";
const int MAX_FIREWALL_INDEX = 87;

int main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    parse_args(argc, argv, &file_handle);

    firewall *firewalls = malloc(sizeof (firewall) * MAX_FIREWALL_INDEX);
    int valid[MAX_FIREWALL_INDEX];
    for(int i=0; i<MAX_FIREWALL_INDEX; i++) {
        valid[i] = 0;
    }
    int firewalls_read = read_input(file_handle, firewalls, valid);
    printf("Read in %d firewalls.\n", firewalls_read);

    int good_delay = delay_to_dodge_firewalls(firewalls, valid, MAX_FIREWALL_INDEX);
    printf("Delay to dodge all firewalls: %d\n", good_delay);
    free(firewalls);

    return 0;
}

int delay_to_dodge_firewalls(firewall *firewalls,
                             int valid[],
                             int num_firewalls) {
    int return_value = -1;
    firewall *initial_state = malloc(sizeof (firewall) * MAX_FIREWALL_INDEX);
    reset_firewall_states(initial_state, valid, num_firewalls, firewalls);

    clock_t start = clock();
    for(int i=0; i<=1000000000; i++) {
        int severity_score = travel_firewalls(firewalls,
                                              valid,
                                              num_firewalls,
                                              initial_state);
        if(severity_score == 0) {
            return_value = i;
            break;
        }
        else {
            if(i && i%1000000 == 0) {
                clock_t now = clock();
                float time_passed = (float) (now - start) / CLOCKS_PER_SEC;
                printf("Failed: %d in %02fs. \n", i, time_passed);
            }
            //printf("Severity with delay %d: %d\n", i, severity_score);
        }
    }

    free(initial_state);
    return return_value;
}

void reset_firewall_states(firewall *firewalls,
                     int valid[],
                     int num_firewalls,
                     firewall *initial_state) {
    if(initial_state != NULL) {
        for(int i=0; i<num_firewalls; i++) {
            if(valid[i]) {
                memcpy((firewalls + i), (initial_state + i), sizeof (firewall));
            }
        }

    }
    else {
        for(int i=0; i<num_firewalls; i++) {
            if(valid[i]) {
                (firewalls + i)->direction = 1;
                (firewalls + i)->ypos = 0;
            }
        }
    }
}

int travel_firewalls(firewall *firewalls,
                     int valid[],
                     int num_firewalls,
                     firewall *initial_state) {
    // Reset the simulation
    reset_firewall_states(firewalls, valid, num_firewalls, initial_state);

    int delayed_attempt = initial_state != NULL;
    int severity_score = 0;
    int total_severity_score = 0;
    int xpos = -1;
    while(xpos < MAX_FIREWALL_INDEX) {
        xpos++;

        // Trip the wires
        if(valid[xpos]) {
            firewall *fire = (firewalls + xpos);
            if(fire->ypos == 0) {
                // We're only interested in success/failure
                if(delayed_attempt) {
                    total_severity_score = -1;
                }
                else {
                    severity_score = fire->range * xpos;
                    total_severity_score += severity_score;
                }
            }
        }

        // Move the scanners
        for(int i=xpos; i<MAX_FIREWALL_INDEX; i++) {
            if(valid[i]) {
                move_scanner((firewalls + i));
            }
        }

        // Cache firewall state
        if(xpos == 0) {
            reset_firewall_states(initial_state, valid, num_firewalls, firewalls);
        }

        if(delayed_attempt && total_severity_score == -1) {
            break;
        }
    }
    return total_severity_score;
}

void move_scanner(firewall *fire) {
    if((fire->direction == 1 && fire->ypos >= fire->range - 1) ||
       (fire->direction == -1 && fire->ypos == 0)) {
        fire->direction *= -1;
    }
    fire->ypos += fire->direction;
}

int read_input(FILE *file_handle, firewall *firewalls, int valid[]) {
    char *linebuf = malloc(sizeof(char) * 128);
    size_t read;
    int num_firewalls = 0;

    while(getline(&linebuf, &read, file_handle) != -1) {
        num_firewalls++;
        int fw_position = atoi(strtok(linebuf, " "));
        int fw_range = atoi(strtok(NULL, " "));
        if(!valid[fw_position]) {
            valid[fw_position] = 1;
            firewall tmp_fire;
            tmp_fire.direction = 1;
            tmp_fire.xpos = fw_position;
            tmp_fire.ypos = 0;
            tmp_fire.range = fw_range;
            *(firewalls + fw_position) = tmp_fire;
        }
        else {
            printf("Oops, %d already validated!", fw_position);
            printf("%s\n", "That wasn't supposed to happen...");
            exit(1);
        }
    }
    free(linebuf);
    fclose(file_handle);

    return num_firewalls;
}

void parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage();
    }

    if((*file_handle = fopen(argv[1], "r")) == 0) {
        perror("Failed to open input file");
        exit(1);
    }
}

void usage() {
    printf("Usage: %s inputfile\n", PROGRAM_NAME);
    exit(1);
}
