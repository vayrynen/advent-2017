/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

  //            //
 /* Prototypes */
//            //
void parse_args(int, char *[], FILE**);
void usage(void);
int read_input(FILE*, int*);
void add_instruction(char[], int*);
void dance(char[], int, int*, int);

  //           //
 /* Constants */
//           //

//#define DEBUG

const char * const PROGRAM_NAME = "dance";
const int MAX_INSTRUCTIONS = 10000;
const int INSTRUCTION_LENGTH = 3;
#ifdef DEBUG
const int NUM_DANCERS = 5;
#else
const int NUM_DANCERS = 16;
#endif
const int NUM_ROUNDS = 1000000000;

const int SPIN = (int) 's';
const int SWAP_POS = (int) 'x';
const int SWAP_NAM = (int) 'p';

int main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    int *instructions = malloc(sizeof (int) *
                               MAX_INSTRUCTIONS *
                               INSTRUCTION_LENGTH);

    parse_args(argc, argv, &file_handle);
    int num_instructions = read_input(file_handle, instructions);
#ifdef DEBUG
    printf("Read and understood %d instructions.\n", num_instructions);
    for(int i=0; i<num_instructions; i++) {
        printf("Instruction %d: %d %d %d\n", i,
                 *(instructions + 3*i),
                 *(instructions + 3*i + 1),
                 *(instructions + 3*i + 2));
    }
#endif
    char dancers[NUM_DANCERS+2];
    for(char c=0; c<NUM_DANCERS; c++) {
        dancers[(int)c] = c + 0x61;
    }
    dancers[NUM_DANCERS] = '\n';
    dancers[NUM_DANCERS+1] = '\0';

    char start_state[NUM_DANCERS+2];
    for(int i=0; i<NUM_DANCERS+2; i++) {
        start_state[i] = dancers[i];
    }

#ifdef DEBUG
    dance(dancers, NUM_DANCERS, instructions, num_instructions);
#else
    time_t start_time = clock();

    int cycle_cadence = 0;
    for(int i=0; i<NUM_ROUNDS; i++) {
        dance(dancers, NUM_DANCERS, instructions, num_instructions);
        if((i + 1) % 1000000 == 0) {
            printf("Danced %d million times in %fs.\n",
            i/1000000,
            (float) (clock() - start_time) / CLOCKS_PER_SEC);
        }
        if(!cycle_cadence) {
            if(strcmp(dancers, start_state) == 0) {
                printf("Cycle closed at %d.\n", i);
                cycle_cadence = i + 1;
                int rounds_left = NUM_ROUNDS - i + 1;
                int skip = (rounds_left/cycle_cadence) * cycle_cadence;
                printf("Skipping %d rounds.\n", skip);
                i += skip;
            }
        }
    }
#endif

    printf("%s", dancers);
    free(instructions);

    return 0;
}

void dance(char dancers[], int num_dancers, int *instructions, int num_instructions) {

    int offset_amount = 0;
    for(int i=0; i<num_instructions; i++) {
        char instruction_type = (char) *(instructions + 3*i);
        int operand_1 = *(instructions + 3*i + 1);
        int operand_2 = *(instructions + 3*i + 2);

        if(instruction_type == SPIN) {
            offset_amount = (offset_amount + operand_1) % num_dancers;
        }
        else if(instruction_type == SWAP_POS) {
            int pos_1 = (operand_1 + num_dancers - offset_amount) % num_dancers;
            int pos_2 = (operand_2 + num_dancers - offset_amount) % num_dancers;

            char tmp = dancers[pos_1];
            dancers[pos_1] = dancers[pos_2];
            dancers[pos_2] = tmp;
        }
        else if(instruction_type == SWAP_NAM) {
            char swap_1 = (char) operand_1;
            char swap_2 = (char) operand_2;
            for(int j=0; j<num_dancers; j++) {
                if(dancers[j] == swap_1) {
                    dancers[j] = swap_2;
                }
                else if(dancers[j] == swap_2) {
                    dancers[j] = swap_1;
                }
            }
        }
        else {
            fprintf(stderr, "Error executing instruction: unknown "
                            "instruction type (%c). Bailing out.\n",
                            (char) instruction_type);
            exit(1);
        }
    }
    char tmp[num_dancers];
    strncpy(tmp, dancers, num_dancers);
    for(int j=0; j<num_dancers; j++) {
        dancers[j] = tmp[(j+num_dancers-offset_amount)%num_dancers];
    }
}

int read_input(FILE *file_handle, int *instructions) {
    int instructions_read = 0;
    char c;
    char instruction_buf[7];
    int buf_index = 0;
    while(1) {
        c = fgetc(file_handle);
        if(c == ',' || c == EOF) {
            instruction_buf[buf_index] = '\0';
            add_instruction(instruction_buf,
                            (instructions + 3*instructions_read++));
            if(c == EOF) {
                break;
            }
            buf_index = 0;
        }
        else {
            instruction_buf[buf_index++] = c;
        }
    }
    fclose(file_handle);

    return instructions_read;
}

void add_instruction(char instruction_buf[], int *instruction_ptr) {
    int instruction = (int) instruction_buf[0];
    *(instruction_ptr) = instruction;

    if(instruction == SPIN) {
        *(instruction_ptr + 1) = atoi(&instruction_buf[1]);
        *(instruction_ptr + 2) = 0;
    }
    else if(instruction == SWAP_POS) {
        *(instruction_ptr + 1) = atoi(&instruction_buf[1]);
        char *slash_index = strstr(instruction_buf, "/");
        *(instruction_ptr + 2) = atoi(slash_index + 1);
    }
    else if(instruction == SWAP_NAM) {
        *(instruction_ptr + 1) = instruction_buf[1];
        *(instruction_ptr + 2) = instruction_buf[3];
    }
    else {
        fprintf(stderr, "Error reading instruction: unknown instruction type "
                        "(%c). Bailing out.\n",
                        (char) instruction);
        exit(1);
    }
}

void parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage();
    }

    char *filename;
#ifdef DEBUG
    filename = "test_input";
#else
    filename = argv[1];
#endif
    if((*file_handle = fopen(filename, "r")) == 0) {
        perror("Failed to open input file");
        exit(1);
    }
}

void usage() {
    printf("Usage: %s inputfile\n", PROGRAM_NAME);
    exit(1);
}
