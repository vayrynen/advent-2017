/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#define _GNU_SOURCE
//#define DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>

  //            //
 /* Prototypes */
//            //

enum instruction_type {
    SND,
    SET,
    ADD,
    MUL,
    MOD,
    RCV,
    JGZ
};

enum value_type {
    REGISTER,
    IMMEDIATE
};

void parse_args(int, char *[], FILE**);
void usage(char*);
int read_input(FILE*, int*);
int play(int*, long*, int, int, int, int*, int*);
enum instruction_type parse_instruction_type(char*);

  //           //
 /* Constants */
//           //
const int MAX_INSTRUCTIONS = 64;
const int INSTRUCTION_WIDTH = 4;
const int MAX_REGISTERS = (int) 'z' - (int) 'a';

  //           //
 /* Functions */
//           //
int main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    parse_args(argc, argv, &file_handle);

    // Instruction -> instruction type, arg1, value type, arg2
    int *instructions = malloc(sizeof(int) * MAX_INSTRUCTIONS * INSTRUCTION_WIDTH);

    long *registers = calloc(MAX_REGISTERS, sizeof(long));
    int num_instructions = read_input(file_handle, instructions);

    // Parent reads from queues[0][0] and writes to queues[1][1]
    // child reads from queues[1][0] and writes to queues[0][1]
    int queues[2][2];
    for(int i=0; i<2; i++) {
        if(pipe2(queues[i], O_NONBLOCK) == -1) {
            perror("pipe");
            exit(1);
        }
    }

    // Create shared memory to enable deadlock detection
    int protection = PROT_READ | PROT_WRITE;
    int flags = MAP_ANONYMOUS | MAP_SHARED;
    int deadlock_status_size = sizeof(int) * 2;
    int *deadlock_status = mmap(NULL, deadlock_status_size, protection, flags, 0, 0);

    memset(deadlock_status, 0, deadlock_status_size);

    int pid;
    if((pid = fork()) == -1) {
        perror("fork");
        exit(1);
    }

    if(pid == 0) {
        *(registers + ('p' % MAX_REGISTERS)) = 1;
    }

    if(pid == 0) {
        int num_sent = play(instructions,
                            registers,
                            num_instructions,
                            queues[1][0],
                            queues[0][1],
                            deadlock_status,
                            deadlock_status + 1);
        printf("Program 1 (child) sent %d values.\n", num_sent);
        close(queues[1][0]);
        close(queues[0][1]);
    }
    else {
        play(instructions,
            registers,
            num_instructions,
            queues[0][0],
            queues[1][1],
            deadlock_status + 1,
            deadlock_status);
        close(queues[0][0]);
        close(queues[1][1]);
    }

    return 0;
}

int play(int *instructions,
        long *registers,
        int num_instructions,
        int read_pipe,
        int write_pipe,
        int *my_deadlock_status,
        int *coprocess_deadlock_status) {

#ifdef DEBUG
    for(int i=0; i<num_instructions; i++) {
        printf("#%d **** Ins: %d par1: %d valtype: %d par2: %d\n",
               i,
               *(instructions + i*INSTRUCTION_WIDTH),
               *(instructions + i*INSTRUCTION_WIDTH + 1),
               *(instructions + i*INSTRUCTION_WIDTH + 2),
               *(instructions + i*INSTRUCTION_WIDTH + 3));
    }
#endif

    enum instruction_type instruction_type;
    enum value_type value_type;
    int reg_index, other_reg_index;

    long optional_arg = -1;
    long values_sent = 0;
    int instr_index = 0;

    while(1) {
        instruction_type = *(instructions + instr_index*INSTRUCTION_WIDTH);
        reg_index = *(instructions + instr_index*INSTRUCTION_WIDTH + 1);
        value_type = *(instructions + instr_index*INSTRUCTION_WIDTH + 2);

        switch(value_type) {
            case REGISTER:
                other_reg_index = *(instructions + instr_index*INSTRUCTION_WIDTH + 3);
                optional_arg = *(registers + other_reg_index);
                break;
            case IMMEDIATE:
                optional_arg = *(instructions + instr_index*INSTRUCTION_WIDTH + 3);
                break;
        }

        int res;
        switch(instruction_type) {
            case SND:
#ifdef DEBUG
                printf("SND %d %lu\n", reg_index, optional_arg);
#endif
                res = write(write_pipe, &optional_arg, sizeof(optional_arg));
                if(res == -1) {
                    perror("write");
                    exit(1);
                }

                values_sent++;
                break;
            case SET:
#ifdef DEBUG
                printf("SET %d %lu\n", reg_index, optional_arg);
#endif
                *(registers + reg_index) = optional_arg;
                break;
            case ADD:
#ifdef DEBUG
                printf("ADD %d %lu\n", reg_index, optional_arg);
#endif
                *(registers + reg_index) += optional_arg;
                break;
            case MUL:
#ifdef DEBUG
                printf("MUL %d %lu\n", reg_index, optional_arg);
#endif
                *(registers + reg_index) *= optional_arg;
                break;
            case MOD:
#ifdef DEBUG
                printf("MOD %d %lu\n", reg_index, optional_arg);
#endif
                *(registers + reg_index) %= optional_arg;
                break;
            case RCV:
#ifdef DEBUG
                printf("RCV %d %lu\n", reg_index, optional_arg);
#endif
                while(1) {
                    res = read(read_pipe, &optional_arg, sizeof(optional_arg));
                    if(res == -1) {
                        if(errno == EAGAIN) {
                            *my_deadlock_status = 1;
                            if(*coprocess_deadlock_status == 1) {
                                fprintf(stderr, "Deadlock detected (read), exiting...\n");
                                return values_sent;
                            }
                        }
                        else {
                            perror("read");
                            exit(1);
                        }
                    }
                    else {
                        *my_deadlock_status = 0;
                        *(registers + reg_index) = optional_arg;
                        break;
                    }
                }

                break;
            case JGZ:
#ifdef DEBUG
                printf("JGZ %d %lu\n", reg_index, optional_arg);
#endif
                // -1 case is a TERRIBLE BAD HACK
                if(reg_index == -1 || *(registers + reg_index) > 0) {
                    // And don't forget the -1 since we increment the index
                    // at the end of the iteration regardless.
                    instr_index += optional_arg - 1;
                }
                break;
        }

        instr_index++;

        if(instr_index < 0 || instr_index > num_instructions) {
            fprintf(stderr, "Instruction index out of bounds, terminating.\n");
            break;
        }
    }

#ifdef DEBUG
    for(int i=0; i<MAX_REGISTERS; i++) {
        printf("Reg #%d: %lu ", i, *(registers + i));
    }
    printf("\n");
#endif

    return values_sent;
}

// Ain't gonna switch-case no strings in C.
enum instruction_type parse_instruction_type(char *instruction_name) {
    if(strcmp(instruction_name, "snd") == 0) {
        return SND;
    }
    else if(strcmp(instruction_name, "set") == 0) {
        return SET;
    }
    else if(strcmp(instruction_name, "add") == 0) {
        return ADD;
    }
    else if(strcmp(instruction_name, "mul") == 0) {
        return MUL;
    }
    else if(strcmp(instruction_name, "mod") == 0 ) {
        return MOD;
    }
    else if(strcmp(instruction_name, "rcv") == 0) {
        return RCV;
    }
    else if(strcmp(instruction_name, "jgz") == 0) {
        return JGZ;
    }
    else {
        return -1;
    }
}

int read_input(FILE *file_handle, int *instructions) {
    char *linebuf = malloc(sizeof(char) * 128);

    int instructions_read = 0;

    char *endptr;
    char *instruction_name, *optional_arg_str;
    enum instruction_type instruction_type;
    enum value_type value_type;
    int reg_index, optional_arg;
    while(fgets(linebuf, 128, file_handle) &&
            instructions_read < MAX_INSTRUCTIONS) {
        instruction_name = strtok(linebuf, " ");
        instruction_type = parse_instruction_type(instruction_name);
        if(instruction_type == -1) {
            fprintf(stderr, "Invalid instruction type: %s\n", instruction_name);
            exit(1);
        }

        *(instructions + instructions_read*INSTRUCTION_WIDTH) = instruction_type;

        switch(instruction_type) {
            case SND:
                reg_index = -1;
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
            case SET:
                reg_index = (int) *(strtok(NULL, " "));
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
            case ADD:
                reg_index = (int) *(strtok(NULL, " "));
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
            case MUL:
                reg_index = (int) *(strtok(NULL, " "));
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
            case MOD:
                reg_index = (int) *(strtok(NULL, " "));
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
            case RCV:
                reg_index = (int) *(strtok(NULL, "\n"));
                value_type = IMMEDIATE;
                optional_arg = INT_MIN;
                break;
            case JGZ:
                // BEGIN TERRIBLE HACK FOR "SPECIAL JGZ CASE"
                optional_arg_str = strtok(NULL, " ");
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr != optional_arg_str) {
                    reg_index = -1;
                }
                // END TERRIBLE HACK
                else {
                    reg_index = (int) *optional_arg_str;
                }

                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
        }

        reg_index = reg_index % MAX_REGISTERS;
        *(instructions + instructions_read*INSTRUCTION_WIDTH + 1) = reg_index;
        *(instructions + instructions_read*INSTRUCTION_WIDTH + 2) = value_type;
        if(value_type == REGISTER) {
            optional_arg = optional_arg % MAX_REGISTERS;
        }
        *(instructions + instructions_read*INSTRUCTION_WIDTH + 3) = optional_arg;

        instructions_read++;
    }
    free(linebuf);
    fclose(file_handle);

    return instructions_read;
}

void parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage(argv[0]);
    }

    if((*file_handle = fopen(argv[1], "r")) == 0) {
        perror("Failed to open input file");
        exit(1);
    }
}

void usage(char *program_name) {
    printf("Usage: %s inputfile\n", program_name);
    exit(1);
}
