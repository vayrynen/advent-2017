/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

//#define DEBUG

  //            //
 /* Prototypes */
//            //

enum instruction_type {
    SND,
    SET,
    ADD,
    MUL,
    MOD,
    RCV,
    JGZ
};

enum value_type {
    REGISTER,
    IMMEDIATE
};

void parse_args(int, char *[], FILE**);
void usage(char*);
int read_input(FILE*, int*);
long play(int*, long*, int);
enum instruction_type parse_instruction_type(char*);

  //           //
 /* Constants */
//           //
const int MAX_INSTRUCTIONS = 64;
const int INSTRUCTION_WIDTH = 4;
const int MAX_REGISTERS = (int) 'z' - (int) 'a';

  //           //
 /* Functions */
//           //
int main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    parse_args(argc, argv, &file_handle);

    // Instruction -> instruction type, arg1, value type, arg2
    int *instructions = malloc(sizeof(int) * MAX_INSTRUCTIONS * INSTRUCTION_WIDTH);

    long *registers = calloc(MAX_REGISTERS, sizeof(long));
    int num_instructions = read_input(file_handle, instructions);

    long sound_recovered = play(instructions, registers, num_instructions);

    printf("Sound recovered was: %ld\n", sound_recovered);

    return 0;
}

long play(int *instructions, long *registers, int num_instructions) {

#ifdef DEBUG
    for(int i=0; i<num_instructions; i++) {
        printf("#%d **** Ins: %d par1: %d valtype: %d par2: %d\n",
               i,
               *(instructions + i*INSTRUCTION_WIDTH),
               *(instructions + i*INSTRUCTION_WIDTH + 1),
               *(instructions + i*INSTRUCTION_WIDTH + 2),
               *(instructions + i*INSTRUCTION_WIDTH + 3));
    }
#endif

    enum instruction_type instruction_type;
    enum value_type value_type;
    int reg_index, other_reg_index;

    long optional_arg = -1;
    long last_sound = -1;
    int instr_index = 0;
    int received = 0;

    while(!received) {
        instruction_type = *(instructions + instr_index*INSTRUCTION_WIDTH);
        reg_index = *(instructions + instr_index*INSTRUCTION_WIDTH + 1);
        value_type = *(instructions + instr_index*INSTRUCTION_WIDTH + 2);

        switch(value_type) {
            case REGISTER:
                other_reg_index = *(instructions + instr_index*INSTRUCTION_WIDTH + 3);
                optional_arg = *(registers + other_reg_index);
                break;
            case IMMEDIATE:
                optional_arg = *(instructions + instr_index*INSTRUCTION_WIDTH + 3);
                break;
        }

        switch(instruction_type) {
            case SND:
#ifdef DEBUG
                printf("SND %d %lu\n", reg_index, optional_arg);
#endif
                last_sound = *(registers + reg_index);
                break;
            case SET:
#ifdef DEBUG
                printf("SET %d %lu\n", reg_index, optional_arg);
#endif
                *(registers + reg_index) = optional_arg;
                break;
            case ADD:
#ifdef DEBUG
                printf("ADD %d %lu\n", reg_index, optional_arg);
#endif
                *(registers + reg_index) += optional_arg;
                break;
            case MUL:
#ifdef DEBUG
                printf("MUL %d %lu\n", reg_index, optional_arg);
#endif
                *(registers + reg_index) *= optional_arg;
                break;
            case MOD:
#ifdef DEBUG
                printf("MOD %d %lu\n", reg_index, optional_arg);
#endif
                *(registers + reg_index) %= optional_arg;
                break;
            case RCV:
#ifdef DEBUG
                printf("RCV %d %lu\n", reg_index, optional_arg);
#endif
                if(*(registers + reg_index)) {
                    received = 1;
                }
                break;
            case JGZ:
#ifdef DEBUG
                printf("JGZ %d %lu\n", reg_index, optional_arg);
#endif
                if(*(registers + reg_index) > 0) {
                    // And don't forget the -1 since we increment the index
                    // at the end of the iteration regardless.
                    instr_index += optional_arg - 1;
                }
                break;
        }

        instr_index++;

        if(instr_index < 0 || instr_index > num_instructions) {
            fprintf(stderr, "Instruction index out of bounds, terminating.");
            exit(1);
        }
    }

#ifdef DEBUG
    for(int i=0; i<MAX_REGISTERS; i++) {
        printf("Reg #%d: %lu ", i, *(registers + i));
    }
    printf("\n");
#endif

    return last_sound;
}

// Ain't gonna switch-case no strings in C.
enum instruction_type parse_instruction_type(char *instruction_name) {
    if(strcmp(instruction_name, "snd") == 0) {
        return SND;
    }
    else if(strcmp(instruction_name, "set") == 0) {
        return SET;
    }
    else if(strcmp(instruction_name, "add") == 0) {
        return ADD;
    }
    else if(strcmp(instruction_name, "mul") == 0) {
        return MUL;
    }
    else if(strcmp(instruction_name, "mod") == 0 ) {
        return MOD;
    }
    else if(strcmp(instruction_name, "rcv") == 0) {
        return RCV;
    }
    else if(strcmp(instruction_name, "jgz") == 0) {
        return JGZ;
    }
    else {
        return -1;
    }
}

int read_input(FILE *file_handle, int *instructions) {
    char *linebuf = malloc(sizeof(char) * 128);

    int instructions_read = 0;

    char *endptr;
    char *instruction_name, *optional_arg_str;
    enum instruction_type instruction_type;
    enum value_type value_type;
    int reg_index, optional_arg;
    while(fgets(linebuf, 128, file_handle) &&
            instructions_read < MAX_INSTRUCTIONS) {
        instruction_name = strtok(linebuf, " ");
        instruction_type = parse_instruction_type(instruction_name);
        if(instruction_type == -1) {
            fprintf(stderr, "Invalid instruction type: %s\n", instruction_name);
            exit(1);
        }

        *(instructions + instructions_read*INSTRUCTION_WIDTH) = instruction_type;

        switch(instruction_type) {
            case SND:
                reg_index = (int) *(strtok(NULL, "\n"));
                value_type = IMMEDIATE;
                optional_arg = INT_MIN;
                break;
            case SET:
                reg_index = (int) *(strtok(NULL, " "));
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
            case ADD:
                reg_index = (int) *(strtok(NULL, " "));
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
            case MUL:
                reg_index = (int) *(strtok(NULL, " "));
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
            case MOD:
                reg_index = (int) *(strtok(NULL, " "));
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
            case RCV:
                reg_index = (int) *(strtok(NULL, "\n"));
                value_type = IMMEDIATE;
                optional_arg = INT_MIN;
                break;
            case JGZ:
                reg_index = (int) *(strtok(NULL, " "));
                optional_arg_str = strtok(NULL, "\n");
#ifdef DEBUG
                printf("Converting \"%s\" to int\n", optional_arg_str);
#endif
                optional_arg = strtol(optional_arg_str, &endptr, 10);
                if(endptr == optional_arg_str) {
                    value_type = REGISTER;
                    optional_arg = (int) *endptr;
                }
                else {
                    value_type = IMMEDIATE;
                }
                break;
        }

        reg_index = reg_index % MAX_REGISTERS;
        *(instructions + instructions_read*INSTRUCTION_WIDTH + 1) = reg_index;
        *(instructions + instructions_read*INSTRUCTION_WIDTH + 2) = value_type;
        if(value_type == REGISTER) {
            optional_arg = optional_arg % MAX_REGISTERS;
        }
        *(instructions + instructions_read*INSTRUCTION_WIDTH + 3) = optional_arg;

        instructions_read++;
    }
    free(linebuf);
    fclose(file_handle);

    return instructions_read;
}

void parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage(argv[0]);
    }

    if((*file_handle = fopen(argv[1], "r")) == 0) {
        perror("Failed to open input file");
        exit(1);
    }
}

void usage(char *program_name) {
    printf("Usage: %s inputfile\n", program_name);
    exit(1);
}
