/*
 * Algorithm idea:
 * -> Nuke all exclamation marks and following characters
 * -> Nuke everything that is not one of <>{} (even commas)
 * -> Nuke everything contained within <>
 *
 * Now only groups of {}, should remain.
 * -> Start with a score_base of 0
 * -> For every { encountered, increase score_base with 1
 * -> For every } encountered, add score_base to score
 *    and decrease score_base with 1
 */


  //          //
 /* Includes */
//          //
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


  //            //
 /* Prototypes */
//            //
void parse_args(int, char *argv[], FILE **);
void usage();
int calculate_group_score(char *);
void read_initial_data(char *, FILE *);
void destroy_canceled_characters(char *, char*);
void destroy_random_characters(char *, char*);
void destroy_garbage_characters(char *, char*);
char * cleanup_data(FILE *);

  //         //
 /* Statics */
//         //
const int INPUT_MAX = 32768;


  //           //
 /* The stuff */
//           //
int main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    parse_args(argc, argv, &file_handle);
    char *clean_data = cleanup_data(file_handle);
    int group_score = calculate_group_score(clean_data);
    free(clean_data);
    printf("Value of all groups: %d\n", group_score);

    return 0;
}

int calculate_group_score(char *clean_data) {
    int base_score = 0;
    int total_score = 0;
    while(*clean_data) {
        if(*(clean_data) == '{') {
            base_score++;
        }
        else if(*(clean_data) == '}') {
            total_score += base_score;
            base_score--;
        }
        clean_data++;
    }
    return total_score;
}

char * cleanup_data(FILE *file_handle) {
    char *initial_data = (char *) malloc(sizeof(char) * INPUT_MAX);
    read_initial_data(initial_data, file_handle);
    printf("Read in %ld bytes of data.\n", strlen(initial_data));

    char *non_canceled_data = (char *) malloc(sizeof(char) * INPUT_MAX);
    destroy_canceled_characters(non_canceled_data, initial_data);
    free(initial_data);
    printf("%ld bytes of data remaining after removing "
           "canceled characters.\n", strlen(non_canceled_data));

    char *garbageless_data = (char *) malloc(sizeof(char) * INPUT_MAX);
    destroy_garbage_characters(garbageless_data, non_canceled_data);
    free(non_canceled_data);
    printf("%ld bytes of data remaining after removing "
           "garbage characters.\n", strlen(garbageless_data));

    char *clean_data = (char *) malloc(sizeof(char) * INPUT_MAX);
    destroy_random_characters(clean_data, garbageless_data);
    free(garbageless_data);
    printf("%ld bytes of data remaining after removing "
           "canceled characters.\n", strlen(clean_data));

    return clean_data;
}

void destroy_random_characters(char *non_random_data, char *non_canceled_data) {
    while(*non_canceled_data) {
        if(*non_canceled_data == '<' || *non_canceled_data == '>' ||
           *non_canceled_data == '{' || *non_canceled_data == '}') {
            *(non_random_data++) = *(non_canceled_data++);
        }
        else {
            non_canceled_data++;
        }
    }
    *non_random_data = '\0';
}

void destroy_garbage_characters(char *clean_data, char *non_random_data) {
    int garbage_destroyed = 0;
    while(*non_random_data) {
        if(*non_random_data == '<') {
            non_random_data++;
            while(*(non_random_data++) != '>') {
                garbage_destroyed++;
            }
        }
        else {
            *(clean_data++) = *(non_random_data)++;
        }
    }
    printf("Destroyed %d garbage characters.\n", garbage_destroyed);
    *clean_data = '\0';
}

void destroy_canceled_characters(char *non_canceled_data, char *initial_data) {
    while(*initial_data) {
        if(*initial_data == '!') {
            initial_data += 2;
        }
        else {
            *(non_canceled_data++) = *(initial_data++);
        }
    }
    *non_canceled_data = '\0';
}

void read_initial_data(char *initial_data, FILE *file_handle) {
    if(fgets(initial_data, INPUT_MAX, file_handle) == 0) {
        perror("Failed to read data from file");
        exit(1);
    }
    fclose(file_handle);
}

void parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage();
    }

    if((*file_handle = fopen(argv[1], "r")) == 0) {
        perror("I/O error");
        exit(1);
    }
}

void usage() {
    char *output_string = "Usage: residue_processing <inputfile>";
    printf("%.*s\n", (int) strlen(output_string), output_string);
    exit(1);
}
