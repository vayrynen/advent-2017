/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

  //            //
 /* Prototypes */
//            //
void usage(char*);
int read_input(FILE*);
void parse_args(int, char*[]);

  //           //
 /* Constants */
//           //
const int SPINS = 363;

#define TASK 2

#if TASK == 1
const int MAX_NUMBERS = 2018;
const int TARGET_VALUE = 2017;
#elif TASK == 2
const int MAX_NUMBERS = 50000000;
const int TARGET_VALUE = 0;
#endif

int main(int argc, char *argv[]) {
    parse_args(argc, argv);

    int *numbers = malloc(sizeof(int) * MAX_NUMBERS);
    int pos = 0;
    *(numbers + pos++) = 0;

    for(int i=1; i<MAX_NUMBERS; i++) {
        pos = (pos + SPINS) % i;
#if TASK == 1
        for(int j=i; j>pos; j--) {
            *(numbers + j) = *(numbers + j - 1);
        }
#endif
        *(numbers + ++pos) = i;
        if(i % 10000 == 0) {
            printf("Done with %d...\n", i);
        }
    }

    for(int i=0; i<MAX_NUMBERS; i++) {
        if(*(numbers + i) == TARGET_VALUE) {
            printf("After %d: %d\n", TARGET_VALUE, *(numbers + (i + 1) % MAX_NUMBERS));
            break;
        }
    }

    free(numbers);
    return 0;
}

void parse_args(int argc, char *argv[]) {
    if(argc != 1) {
        usage(argv[0]);
    }
}

void usage(char *program_name) {
    printf("Usage: %s inputfile\n", program_name);
    exit(1);
}
