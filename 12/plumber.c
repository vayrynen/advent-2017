/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

  //            //
 /* Prototypes */
//            //
typedef struct program_with_pipes pwp;

void parse_args(int, char *[], FILE**);
void usage(void);
int read_progs(FILE*, pwp*);
void print_programs(pwp*, int);
int connected_pipes(pwp*, int[], int, int, int);
int inarray(int[], int, int);
void add_nonconnected_pipes(pwp, int[], int, int*);
int first_free_nonnegative_integer(int[], int);
void add_nonconnected_pipe(int[], int*, int, int);

  //           //
 /* Constants */
//           //
const char * const PROGRAM_NAME = "plumber";
const int MAX_PROGRAMS = 2000;
const int MAX_PIPES = 5;

  //         //
 /* Structs */
//         //
struct program_with_pipes {
    int pid;
    int num_pipes;
    int pipes[5];
};

int main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    parse_args(argc, argv, &file_handle);

    pwp* progs = malloc(sizeof (pwp) * MAX_PROGRAMS);
    int num_progs = read_progs(file_handle, progs);

    int connections[MAX_PROGRAMS];
    for(int i=0; i<MAX_PROGRAMS; i++) {
        connections[i] = -1;
    }

    int pid_to_check = 0;
    int num_connected = connected_pipes(progs, connections, 0,
                                        num_progs, pid_to_check);

    printf("Connected to pid %d: %d pipes\n", pid_to_check, num_connected);
    for(int i=0; i<num_connected; i++) {
        connections[i] = -1;
    }

    int connection_offset = 0;
    int groups = 0;
    do {
        pid_to_check = first_free_nonnegative_integer(connections, MAX_PROGRAMS);
        if(pid_to_check != -1) {
            groups++;
            printf("Checking %d, offset: %d\n", pid_to_check, connection_offset);
            connection_offset = connected_pipes(progs,
                                                connections,
                                                connection_offset,
                                                num_progs,
                                                pid_to_check);
        }
    } while(pid_to_check != -1);

    printf("Found %d self-contained groups.\n", groups);

    free(progs);
    return 0;
}

int first_free_nonnegative_integer(int connections[], int max_value) {
    for(int i=0; i<max_value; i++) {
        if(!inarray(connections, max_value, i)) {
            return i;
        }
    }
    return -1;
}

int connected_pipes(pwp* progs,
                    int connections[],
                    int connection_offset,
                    int num_progs,
                    int pid) {
    int num_connected = connection_offset;

    pwp base_pwp = *(progs + pid);
    add_nonconnected_pipe(connections, &num_connected, num_progs, pid);
    add_nonconnected_pipes(base_pwp, connections, num_connected, &num_connected);

    int prev_connected;
    do {
        prev_connected = num_connected;
        for(int i=0; i<num_connected; i++) {
            pwp connected_pwp = *(progs + connections[i]);
            add_nonconnected_pipe(connections, &num_connected,
                                  num_progs, connected_pwp.pid);
            add_nonconnected_pipes(connected_pwp, connections,
                                   MAX_PROGRAMS, &num_connected);
        }
    } while(prev_connected != num_connected);

    return num_connected;
}

void add_nonconnected_pipe(int connections[],
                           int *num_connected,
                           int num_progs,
                           int pid) {
    if(!inarray(connections, num_progs, pid)) {
        connections[(*num_connected)++] = pid;
    }
}

void add_nonconnected_pipes(pwp prog,
                            int connections[],
                            int limit,
                            int *connection_counter) {
    for(int i=0; i<prog.num_pipes; i++) {
        if(!inarray(connections, limit, prog.pipes[i])) {
            connections[(*connection_counter)++] = prog.pipes[i];
        }
    }
}

int inarray(int nums[], int num_elements, int number) {
    for(int i=0; i<num_elements; i++) {
        if(nums[i] == number) {
            return 1;
        }
    }
    return 0;
}

void print_programs(pwp* progs, int num_progs) {
    for(int i=0; i<num_progs; i++) {
        pwp prog = *(progs + i);
        printf("%d <-> ", prog.pid);
        for(int j=0; j<prog.num_pipes; j++) {
            printf("%d", prog.pipes[j]);
            if(j < prog.num_pipes-1) {
                printf(", ");
            }
        }
        printf("\n");
    }
}

int read_progs(FILE* file_handle, pwp* progs) {
    int num_progs = 0;

    size_t read;
    char *linebuf = malloc(sizeof(char) * 128);
    while(getline(&linebuf, &read, file_handle) != -1) {
        int pid = atoi(strtok(linebuf, " "));
        int num_pipes = 0;
        int pipes[MAX_PIPES];
        char *token;

        strtok(NULL, " "); // Throw away
        while((token = strtok(NULL, " ")) != NULL) {
            for(int i=0; i<8; i++) {
                if(*(token + i) == ',' ||
                   *(token + i) == '\n') {
                    *(token + i) = '\0';
                    break;
                }
            }
            pipes[num_pipes++] = atoi(token);
        }

        pwp tmp_pwp;
        tmp_pwp.pid = pid;
        tmp_pwp.num_pipes = num_pipes;
        for(int i=0;i<num_pipes;i++) {
            tmp_pwp.pipes[i] = pipes[i];
        }
        *(progs++) = tmp_pwp;
        num_progs++;
    }
    free(linebuf);
    fclose(file_handle);

    return num_progs;
}

void parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage();
    }

    if((*file_handle = fopen(argv[1], "r")) == 0) {
        perror("Failed to open input file");
        exit(1);
    }
}

void usage() {
    printf("Usage: %s inputfile\n", PROGRAM_NAME);
    exit(1);
}
