/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define DEBUG 0

  //           //
 /* Constants */
//           //
static int NUM_PARTICLES = 1000;

  //            //
 /* Prototypes */
//            //
typedef struct particle {
    int id;
    long x, y, z;
    long xv, yv, zv;
    int xa, ya, za;
    int alive;
} Particle;

void parse_args(int, char *[], FILE**);
void usage(char*);
int read_input(FILE*, Particle[]);
void move_particles(Particle[], int);
int find_particle_nearest_origo(Particle[]);
int find_remaining_particle_count(Particle[]);

  //           //
 /* Functions */
//           //
int main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    parse_args(argc, argv, &file_handle);

    Particle particles[NUM_PARTICLES];
    read_input(file_handle, particles);

    int num_iterations = 100000;
    move_particles(particles, num_iterations);

    int remaining_particles = find_remaining_particle_count(particles);
    printf("Particles remaining: %d\n", remaining_particles);

    return 0;
}

int find_remaining_particle_count(Particle particles[NUM_PARTICLES]) {
    int particles_remaining = 0;
    for(int i=0; i<NUM_PARTICLES; i++) {
        if(particles[i].alive) {
            particles_remaining++;
        }
    }
    return particles_remaining;
}

int find_particle_nearest_origo(Particle particles[NUM_PARTICLES]) {
    int nearest_particle = -1;
    long nearest_origo = -1;

    for(int i=0; i<NUM_PARTICLES; i++) {
        long particle_distance = labs(particles[i].x) +
                                labs(particles[i].y) +
                                labs(particles[i].z);
        if(nearest_origo == -1 || particle_distance < nearest_origo) {
            nearest_origo = particle_distance;
            nearest_particle = i;
        }
    }

    return nearest_particle;
}

void move_particles(Particle particles[NUM_PARTICLES], int num_iterations) {
    for(int i=0; i<num_iterations; i++) {
        for(int j=0; j<NUM_PARTICLES; j++) {
            // Accelerate
            particles[j].xv += particles[j].xa;
            particles[j].yv += particles[j].ya;
            particles[j].zv += particles[j].za;

            // Move
            particles[j].x += particles[j].xv;
            particles[j].y += particles[j].yv;
            particles[j].z += particles[j].zv;
        }
        for(int j=0; j<NUM_PARTICLES; j++) {
            if(particles[j].alive) {
                for(int k=j+1; k<NUM_PARTICLES; k++) {
                    if(particles[k].alive) {
                        if(particles[j].x == particles[k].x &&
                            particles[j].y == particles[k].y &&
                            particles[j].z == particles[k].z)
                        {
                            particles[j].alive = 0;
                            particles[k].alive = 0;
                        }
                    }
                }
            }
        }
    }
}

int read_input(FILE *file_handle, Particle particles[NUM_PARTICLES]) {
    char *linebuf = malloc(sizeof(char) * 128);
    size_t read;

    for(int i=0; i<NUM_PARTICLES; i++) {
        if (getline(&linebuf, &read, file_handle) == -1) {
            if(errno) {
                perror("getline");
            }
            else {
                fprintf(stderr, "Unexpected EOF\n");
            }
            exit(1);
        }

        // Input format: p=<x,y,z>, v=<xv,yv,zv>, a=<xa,ya,za>

        // Discard
        strtok(linebuf, "<");

        particles[i].x = atol(strtok(NULL, ","));
        particles[i].y = atol(strtok(NULL, ","));
        particles[i].z = atol(strtok(NULL, ">"));

        // Discard
        strtok(NULL, "<");

        particles[i].xv = atol(strtok(NULL, ","));
        particles[i].yv = atol(strtok(NULL, ","));
        particles[i].zv = atol(strtok(NULL, ">"));

        // Discard
        strtok(NULL, "<");

        particles[i].xa = atoi(strtok(NULL, ","));
        particles[i].ya = atoi(strtok(NULL, ","));
        particles[i].za = atoi(strtok(NULL, ">"));

        particles[i].alive = 1;

        if(DEBUG) {
            printf("x=%ld, y=%ld, z=%ld, xv=%ld, yv=%ld, zv=%ld, xa=%d, ya=%d, za=%d\n",
                    particles[i].x, particles[i].y, particles[i].z,
                    particles[i].xv, particles[i].yv, particles[i].zv,
                    particles[i].xa, particles[i].ya, particles[i].za);
        }
    }

    free(linebuf);
    fclose(file_handle);

    return -1;
}

void parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage(argv[0]);
    }

    if((*file_handle = fopen(argv[1], "r")) == 0) {
        perror("Failed to open input file");
        exit(1);
    }
}

void usage(char *program_name) {
    printf("Usage: %s inputfile\n", program_name);
    exit(1);
}
