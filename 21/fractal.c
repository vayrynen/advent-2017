/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

  //           //
 /* Constants */
//           //
static short MAX_DIM = 20;
static short GRID_START[3][3] = { { 0, 1, 0 },
                                  { 0, 0, 1 },
                                  { 1, 1, 1 } };
static short NUM_RECIPES_2x2 = 6;
static short NUM_RECIPES_3x3 = 102;

  //            //
 /* Prototypes */
//            //
void parse_args(int, char *[], FILE**);
void usage(char*);
int read_input(FILE*,
               short[NUM_RECIPES_2x2][2*2],
               short[NUM_RECIPES_2x2][3*3],
               short[NUM_RECIPES_3x3][3*3],
               short[NUM_RECIPES_3x3][4*4]);
int enhance_grid(short[MAX_DIM][MAX_DIM],
                 short[MAX_DIM][MAX_DIM],
                 int,
                 short[NUM_RECIPES_2x2][2*2],
                 short[NUM_RECIPES_2x2][3*3],
                 short[NUM_RECIPES_3x3][3*3],
                 short[NUM_RECIPES_3x3][4*4]);
void print_grid(short[MAX_DIM][MAX_DIM], int);
void enhance_2x2_pattern(short[2*2],
                         short[3*3],
                         short[NUM_RECIPES_2x2][2*2],
                         short[NUM_RECIPES_2x2][3*3]);
void enhance_3x3_pattern(short[3*3],
                         short[4*4],
                         short[NUM_RECIPES_3x3][3*3],
                         short[NUM_RECIPES_3x3][4*4]);

int match2(short[2*2], short[2*2]);
int match3(short[3*3], short[3*3]);
int match2_flip_rotate(short[2*2], short[2*2], int);
int match3_flip_rotate(short[3*3], short[3*3], int);

  //           //
 /* Functions */
//           //
int
main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    parse_args(argc, argv, &file_handle);

    short recipes_2x2_in[NUM_RECIPES_2x2][2*2];
    short recipes_2x2_out[NUM_RECIPES_2x2][3*3];

    short recipes_3x3_in[NUM_RECIPES_3x3][3*3];
    short recipes_3x3_out[NUM_RECIPES_3x3][4*4];

    read_input(file_handle,
               recipes_2x2_in,
               recipes_2x2_out,
               recipes_3x3_in,
               recipes_3x3_out);

    short input_grid[MAX_DIM][MAX_DIM];
    short output_grid[MAX_DIM][MAX_DIM];

    for(int i=0;i<MAX_DIM;i++) {
        for(int j=0;j<MAX_DIM;j++) {
            input_grid[i][j] = 0;
            output_grid[i][j] = 0;
        }
    }

    for(int i=0;i<3;i++) {
        for(int j=0;j<3;j++) {
            output_grid[i][j] = GRID_START[i][j];
        }
    }

    int max_iterations = 5;
    int dimension = 3;

    printf("Starting grid:\n");
    print_grid(output_grid, 3);

    for(int iteration=1; iteration<max_iterations+1; iteration++) {
        memcpy(input_grid, output_grid, sizeof(output_grid));
        dimension = enhance_grid(input_grid,
                                 output_grid,
                                 dimension,
                                 recipes_2x2_in,
                                 recipes_2x2_out,
                                 recipes_3x3_in,
                                 recipes_3x3_out);
        printf("Result after %d iterations: \n", iteration);
        print_grid(output_grid, dimension);
    }

    int pixels_on = 0;
    for(int i=0;i<MAX_DIM;i++) {
        for(int j=0;j<MAX_DIM;j++) {
            pixels_on += output_grid[i][j];
        }
    }

    printf("Pixels on after %d iterations: %d\n", max_iterations, pixels_on);

    return 0;
}

void
print_grid(short grid[MAX_DIM][MAX_DIM], int dimension) {
    for(int i=0;i<dimension;i++) {
        for(int j=0;j<dimension;j++) {
            printf("%s", grid[i][j] ? "#" : ".");
        }
        printf("\n");
    }
    printf("\n");
}

/* Enhance input grid and return output dimension */
int
enhance_grid(short input_grid[MAX_DIM][MAX_DIM],
                 short output_grid[MAX_DIM][MAX_DIM],
                 int input_dimension,
                 short recipes_2x2_in[NUM_RECIPES_2x2][2*2],
                 short recipes_2x2_out[NUM_RECIPES_2x2][3*3],
                 short recipes_3x3_in[NUM_RECIPES_3x3][3*3],
                 short recipes_3x3_out[NUM_RECIPES_3x3][4*4])
{
    int output_dimension = 0;
    if(input_dimension % 2 == 0) {
        int grids_2x2 = input_dimension / 2;
        output_dimension = grids_2x2*3;
        for(int i=0;i<grids_2x2;i++) {
            for(int j=0;j<grids_2x2;j++) {
                short inpattern[2*2];
                for(int k=0;k<2;k++) {
                    for(int l=0;l<2;l++) {
                        inpattern[k*2+l] =
                            input_grid[k+2*i][l+2*j];
                    }
                }
                short outpattern[3*3];
                enhance_2x2_pattern(inpattern,
                                    outpattern,
                                    recipes_2x2_in,
                                    recipes_2x2_out);
                for(int k=0;k<3;k++) {
                    for(int l=0;l<3;l++) {
                        output_grid[k+3*i][l+3*j] =
                            outpattern[k*3+l];
                    }
                }
            }
        }
    }
    else {
        int grids_3x3 = input_dimension / 3;
        output_dimension = grids_3x3*4;
        for(int i=0;i<grids_3x3;i++) {
            for(int j=0;j<grids_3x3;j++) {
                short inpattern[3*3];
                for(int k=0;k<3;k++) {
                    for(int l=0;l<3;l++) {
                        inpattern[k*3+l] =
                            input_grid[k+3*i][l+3*j];
                    }
                }
                short outpattern[4*4];
                enhance_3x3_pattern(inpattern,
                                    outpattern,
                                    recipes_3x3_in,
                                    recipes_3x3_out);
                for(int k=0;k<4;k++) {
                    for(int l=0;l<4;l++) {
                        output_grid[k+4*i][l+4*j] =
                            outpattern[k*4+l];
                    }
                }
            }
        }
    }
    return output_dimension;
}

void enhance_2x2_pattern(short input[2*2],
                         short output[3*3],
                         short inpatterns[NUM_RECIPES_2x2][2*2],
                         short outpatterns[NUM_RECIPES_2x2][3*3])
{
    for(int i=0; i<NUM_RECIPES_2x2; i++) {
        if(match2(input, inpatterns[i])) {
            memcpy(output, outpatterns[i], sizeof(outpatterns[i]));
            return;
        }
    }
    fprintf(stderr, "%s\n", "No matches found...");
    exit(1);
}

void enhance_3x3_pattern(short input[3*3],
                         short output[4*4],
                         short inpatterns[NUM_RECIPES_3x3][3*3],
                         short outpatterns[NUM_RECIPES_3x3][4*4])
{
    for(int i=0; i<NUM_RECIPES_3x3; i++) {
        if(match3(input, inpatterns[i])) {
            memcpy(output, outpatterns[i], sizeof(outpatterns[i]));
            return;
        }
    }
    fprintf(stderr, "%s\n", "No matches found...");
    exit(1);
}

int match2(short input[2*2], short pattern[2*2]) {
    short pat[2*2];
    for(int i=0; i<4; i++) {
        memcpy(pat, pattern, sizeof(short int[2*2]));
        if(match2_flip_rotate(input, pat, i)) {
            return 1;
        }
    }
    return 0;
}

int match3(short input[3*3], short pattern[3*3]) {
    short pat[3*3];
    for(int i=0; i<4; i++) {
        memcpy(pat, pattern, sizeof(short int[3*3]));
        if(match3_flip_rotate(input, pat, i)) {
            return 1;
        }
    }
    return 0;
}

int match2_flip_rotate(short input[2*2], short pattern[2*2], int rotations) {
    short tmp[2*2];
    for(int i=0; i<rotations; i++) {
        memcpy(tmp, pattern, sizeof(tmp));
        pattern[0] = tmp[1];
        pattern[1] = tmp[3];
        pattern[2] = tmp[0];
        pattern[3] = tmp[2];
    }

    int good = 1;
    for(int i=0; i<2*2; i++) {
        if(input[i] != pattern[i]) {
            good = 0;
            break;
        }
    }

    if(good) {
        return 1;
    }

    good = 1;

    // Flip it!
    memcpy(tmp, pattern, sizeof(tmp));
    pattern[0] = tmp[1];
    pattern[1] = tmp[0];
    pattern[2] = tmp[3];
    pattern[3] = tmp[2];

    for(int i=0; i<2*2; i++) {
        if(input[i] != pattern[i]) {
            good = 0;
            break;
        }
    }

    return good;
}

int match3_flip_rotate(short input[3*3], short pattern[3*3], int rotations) {
    short tmp[3*3];
    for(int i=0; i<rotations*2; i++) {
        memcpy(tmp, pattern, sizeof(tmp));
        pattern[0] = tmp[1];
        pattern[1] = tmp[2];
        pattern[2] = tmp[5];
        pattern[3] = tmp[0];
        pattern[5] = tmp[8];
        pattern[6] = tmp[3];
        pattern[7] = tmp[6];
        pattern[8] = tmp[7];
    }

    int good = 1;
    for(int i=0; i<3*3; i++) {
        if(input[i] != pattern[i]) {
            good = 0;
            break;
        }
    }

    if(good) {
        return 1;
    }

    good = 1;

    // Flip it!
    memcpy(tmp, pattern, sizeof(tmp));
    for(int i=0;i<3;i++) {
        pattern[i] = tmp[2-i];
    }
    for(int i=0;i<3;i++) {
        pattern[i+3] = tmp[2-i+3];
    }
    for(int i=0;i<3;i++) {
        pattern[i+6] = tmp[2-i+6];
    }

    for(int i=0; i<3*3; i++) {
        if(input[i] != pattern[i]) {
            good = 0;
            break;
        }
    }

    return good;
}

int
read_input(FILE *file_handle,
           short recipes_2x2_in[NUM_RECIPES_2x2][2*2],
           short recipes_2x2_out[NUM_RECIPES_2x2][3*3],
           short recipes_3x3_in[NUM_RECIPES_3x3][3*3],
           short recipes_3x3_out[NUM_RECIPES_3x3][4*4])
{
    char *linebuf = malloc(sizeof(char) * 128);
    size_t read;

    for(int i=0;i<NUM_RECIPES_2x2;i++) {
        if(getline(&linebuf, &read, file_handle) == -1) {
            perror("getline");
            exit(1);
        }

        int recipepos = 0;
        char *cptr = linebuf;
        while(recipepos < 2*2) {
            if(*cptr == '.') {
                recipes_2x2_in[i][recipepos++] = 0;
            }
            else if(*cptr == '#') {
                recipes_2x2_in[i][recipepos++] = 1;
            }
            else if(*cptr == '/') {
                // Skip
            }
            else {
                printf("%s '%c'\n", "Unknown symbol", *cptr);
                exit(1);
            }
            cptr++;
        }

        // Hop over the ASCII arrow
        cptr = cptr + 4;
        recipepos = 0;
        while(recipepos < 3*3) {
            if(*cptr == '.') {
                recipes_2x2_out[i][recipepos++] = 0;
            }
            else if(*cptr == '#') {
                recipes_2x2_out[i][recipepos++] = 1;
            }
            else if(*cptr == '/') {
                // Skip
            }
            else {
                printf("%s '%c'\n", "Unknown symbol", *cptr);
                exit(1);
            }
            cptr++;
        }
    }

    for(int i=0;i<NUM_RECIPES_3x3;i++) {
        if(getline(&linebuf, &read, file_handle) == -1) {
            perror("getline");
            exit(1);
        }

        int recipepos = 0;
        char *cptr = linebuf;
        while(recipepos < 3*3) {
            if(*cptr == '.') {
                recipes_3x3_in[i][recipepos++] = 0;
            }
            else if(*cptr == '#') {
                recipes_3x3_in[i][recipepos++] = 1;
            }
            else if(*cptr == '/') {
                // Skip
            }
            else {
                printf("%s '%c'\n", "Unknown symbol", *cptr);
                exit(1);
            }
            cptr++;
        }

        // Hop over the ASCII arrow
        cptr = cptr + 4;
        recipepos = 0;
        while(recipepos < 4*4) {
            if(*cptr == '.') {
                recipes_3x3_out[i][recipepos++] = 0;
            }
            else if(*cptr == '#') {
                recipes_3x3_out[i][recipepos++] = 1;
            }
            else if(*cptr == '/') {
                // Skip
            }
            else {
                printf("%s '%c'\n", "Unknown symbol", *cptr);
                exit(1);
            }
            cptr++;
        }
    }
    free(linebuf);
    fclose(file_handle);

    return -1;
}

void
parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage(argv[0]);
    }

    if((*file_handle = fopen(argv[1], "r")) == 0) {
        perror("Failed to open input file");
        exit(1);
    }
}

void
usage(char *program_name) {
    printf("Usage: %s inputfile\n", program_name);
    exit(1);
}
