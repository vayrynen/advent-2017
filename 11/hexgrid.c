/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

  //            //
 /* Prototypes */
//            //
void parse_args(int, char* [], FILE**);
void usage();
void read_instructions(FILE*, int*, short*);
void hex_move(short, int*, int*);
void hex_journey(int, short*, int*, int*, int*);
int origo_distance(int, int);

  //           //
 /* Constants */
//           //
const char* const PROGRAM_NAME = "hexgrid";
const int MAX_INSTRUCTIONS = 16384;
const short N = 0;
const short S = 1;
const short NE = 2;
const short NW = 3;
const short SE = 4;
const short SW = 5;

int main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    parse_args(argc, argv, &file_handle);

    int num_instructions;
    short *instructions = (short *) malloc(sizeof(short) * MAX_INSTRUCTIONS);

    read_instructions(file_handle, &num_instructions, instructions);

    if(fclose(file_handle) != 0) {
        perror("Failed to close input file");
    }

    int x = 0, y = 0, maxdist = 0;
    hex_journey(num_instructions, instructions, &x, &y, &maxdist);
    free(instructions);

    printf("x: %d, y: %d\n", x, y);
    printf("Distance to origo: %d steps\n", origo_distance(x, y));
    printf("Max distance to origo: %d steps\n", maxdist);

    return 0;
}

void hex_journey(int num_instructions,
                 short *instructions,
                 int *x,
                 int *y,
                 int *maxdist) {
    for(int i=0; i<num_instructions; i++) {
        hex_move(*(instructions + i), x, y);
        int distance = origo_distance(*x, *y);
        if(distance > *maxdist) {
            (*maxdist) = distance;
        }
    }
}

void hex_move(short instruction, int *x, int *y) {
    switch(instruction) {
        case 0: // N
            (*y)++;
            break;
        case 1: // S
            (*y)--;
            break;
        case 2: // NE
            if((*x) % 2 != 0) {
                (*y)++;
            }
            (*x)++;
            break;
        case 3: // NW
            if((*x) % 2 != 0) {
                (*y)++;
            }
            (*x)--;
            break;
        case 4: // SE
            if((*x) % 2 == 0) {
                (*y)--;
            }
            (*x)++;
            break;
        case 5: // SW
            if((*x) % 2 == 0) {
                (*y)--;
            }
            (*x)--;
            break;
        default:
            printf("Unknown instruction: %d\n", instruction);
            exit(1);
    }
}

int origo_distance(int x, int y) {
    int traveled = 0;

    while(x != 0 || y != 0) {
        if(x == 0) {
            if(y > 0) {     // Going south
                y--;
            }
            else {          // Going north
                y++;
            }
        }
        else if(x > 0) {
            if(y > 0) {     // Going southwest
                if(x % 2 == 0) {
                    y--;
                }
            }
            else {          // Going northwest
                if(x % 2 != 0) {
                    y++;
                }
            }
            x--;
        }
        else {
            if(y > 0) {     // Going southeast
                if(x % 2 == 0) {
                    y--;
                }
            }
            else {          // Going northeast
                if(x % 2 != 0) {
                    y++;
                }
            }
            x++;
        }

        traveled++;
    }

    return traveled;
}

void read_instructions(FILE *file_handle,
                       int *num_instructions,
                       short *instructions) {
    (*num_instructions) = 0;

    char c = 0, prevc = ',';
    short instruction = -1;
    while((c = fgetc(file_handle)) != EOF) {
        switch(c) {
            case 'n':
                instruction = N;
                break;
            case 'e':
                if(prevc == 'n') {
                    instruction = NE;
                }
                else if(prevc == 's') {
                    instruction = SE;
                }
                break;
            case 's':
                instruction = S;
                break;
            case 'w':
                if(prevc == 'n') {
                    instruction = NW;
                }
                else if(prevc == 's') {
                    instruction = SW;
                }
                break;
        }
        prevc = c;
        if(instruction != -1) {
            // S, N
            if(instruction < NE) {
                (*num_instructions)++;
                (*instructions++) = instruction;
            }
            // Else graciously fix our assumption error :)
            else {
                (*(instructions-1)) = instruction;
            }
            instruction = -1;
        }
    }
}

void parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage();
    }

    if((*file_handle = fopen(argv[1], "r")) == 0) {
        perror("Failed to open input file");
        exit(1);
    }
}

void usage() {
    printf("Usage: %s inputfile\n", PROGRAM_NAME);
    exit(1);
}
